//
//  Country.swift
//  itea-08-07
//
//  Created by Валентин Петруля on 7/8/19.
//  Copyright © 2019 Валентин Петруля. All rights reserved.
//

import Foundation

class Country {
    var name: String?
    var currency: [String]?
    var capital: String?
    var population: String?
    var code: String?
    var region: String?
    var square: String?
}
