//
//  SecondViewController.swift
//  itea-08-07
//
//  Created by Валентин Петруля on 7/8/19.
//  Copyright © 2019 Валентин Петруля. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController {
    var string = ""
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var currencyLabel: UILabel!
    @IBOutlet weak var capitalLabel: UILabel!
    @IBOutlet weak var populationLabel: UILabel!
    @IBOutlet weak var codeLabel: UILabel!
    @IBOutlet weak var regionLabel: UILabel!
    @IBOutlet weak var squareLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        parcing()
        //update(country: country)
        // Do any additional setup after loading the view.
    }
    
    func parcing() {
        if let url = URL(string: "https://restcountries-v1.p.rapidapi.com/capital/" + string) {
            var request = URLRequest(url: url)
            request.httpMethod = "GET"
            request.allHTTPHeaderFields = ["X-RapidAPI-Host": "restcountries-v1.p.rapidapi.com", "X-RapidAPI-Key":"d2ffdd934cmsh200c33db0df10c9p1b0b19jsndf2ca739e28d"]
            
            let task = URLSession.shared.dataTask(with: request) {(data, response, error) in
                guard let data = data else {
                    return
                }
                
                let country = Country()
                
                do {
                    if let json = try JSONSerialization.jsonObject(with: data, options: []) as? [[String: Any]] {
                        if let name = json[0]["name"] as? String {
                            country.name = name
                        }
                        if let currency = json[0]["currencies"] as? [String] {
                            country.currency = currency
                        }
                        if let capital = json[0]["capital"] as? String {
                            country.capital = capital
                        }
                        if let population = json[0]["population"] as? Int {
                            country.population = String(population)
                        }
                        if let code = json[0]["numericCode"] as? String {
                            country.code = code
                        }
                        if let region = json[0]["region"] as? String {
                            country.region = region
                        }
                        if let area = json[0]["area"] as? String {
                            country.square = area
                        }
                        self.nameLabel.text = country.name
                        self.currencyLabel.text = country.currency?[0]
                        self.capitalLabel.text = country.capital
                        self.populationLabel.text = country.population
                        self.codeLabel.text = country.code
                        self.regionLabel.text = country.region
                        self.squareLabel.text = country.square
                    }
                } catch let error as NSError {
                    print("Failed to load: \(error.localizedDescription)")
                }
            }
            task.resume()
        }
    }
    
//    func update(country: Country) {
//        nameLabel.text = country.name
//        currencyLabel.text = country.currency?[0]
//        capitalLabel.text = country.capital
//        populationLabel.text = country.population
//        codeLabel.text = country.code
//        regionLabel.text = country.region
//        squareLabel.text = country.square
//    }
}
