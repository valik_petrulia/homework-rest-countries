//
//  FirstViewController.swift
//  itea-08-07
//
//  Created by Валентин Петруля on 7/8/19.
//  Copyright © 2019 Валентин Петруля. All rights reserved.
//

import UIKit

class FirstViewController: UIViewController {
    
    @IBOutlet weak var textField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func didTapShowButton(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "SecondViewController") as! SecondViewController
        vc.string = textField.text ?? ""
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
